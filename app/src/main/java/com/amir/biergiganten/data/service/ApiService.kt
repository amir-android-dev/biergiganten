package com.amir.biergiganten.data.service


import com.amir.biergiganten.data.model.ResponseBeer
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    //fetching all beer
    @GET("beers")
    suspend fun responseBeers(@Query("page") page: Int): Response<ResponseBeer>

    //fetching detail by id
    @GET("beers/{id}")
    suspend fun responseBeerDetail(@Path("id") id: Int): Response<ResponseBeer>

    //search by name
    @GET("beers")
    suspend fun responseSearchByName(@Query("beer_name") name: String): Response<ResponseBeer>

}