package com.amir.biergiganten.data.model


import com.amir.biergiganten.data.model.ResponseBeer.ResponseBeerItem
import com.google.gson.annotations.SerializedName

class ResponseBeer : ArrayList<ResponseBeerItem>() {
    data class ResponseBeerItem(
        @SerializedName("attenuation_level")
        val attenuationLevel: Double?, // 88.9
        @SerializedName("brewers_tips")
        val brewersTips: String?, // The earthy and floral aromas from the hops can be overpowering. Drop a little Cascade in at the end of the boil to lift the profile with a bit of citrus.
        @SerializedName("contributed_by")
        val contributedBy: String?, // Sam Mason <samjbmason>
        @SerializedName("description")
        val description: String?, // A light, crisp and bitter IPA brewed with English and American hops. A small batch brewed only once.
        @SerializedName("first_brewed")
        val firstBrewed: String?, // 09/2007
        @SerializedName("food_pairing")
        val foodPairing: List<String?>?,
        @SerializedName("id")
        val id: Int, // 1
        @SerializedName("image_url")
        val imageUrl: String?, // https://images.punkapi.com/v2/keg.png
        @SerializedName("ingredients")
        val ingredients: Ingredients?,
        @SerializedName("name")
        val name: String?, // Buzz
    ) {

        data class Ingredients(
            @SerializedName("hops")
            val hops: List<Hop?>?,
            @SerializedName("malt")
            val malt: List<Malt?>?,
            @SerializedName("yeast")
            val yeast: String? // Wyeast 1056 - American Ale™
        ) {
            data class Hop(
                @SerializedName("add")
                val add: String?, // start
                @SerializedName("amount")
                val amount: Amount?,
                @SerializedName("attribute")
                val attribute: String?, // bitter
                @SerializedName("name")
                val name: String? // Fuggles
            ) {
                data class Amount(
                    @SerializedName("unit")
                    val unit: String?, // grams
                    @SerializedName("value")
                    val value: Double? // 37.5
                )
            }
            data class Malt(
                @SerializedName("amount")
                val amount: Amount?,
                @SerializedName("name")
                val name: String? // Maris Otter Extra Pale
            ) {
                data class Amount(
                    @SerializedName("unit")
                    val unit: String?, // kilograms
                    @SerializedName("value")
                    val value: Double? // 3.3
                )
            }
        }
    }
}