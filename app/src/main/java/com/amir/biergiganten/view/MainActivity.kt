package com.amir.biergiganten.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import com.amir.biergiganten.R
import com.amir.biergiganten.databinding.ActivityMainBinding
import com.amir.biergiganten.utils.ConnectivityChecker
import com.amir.biergiganten.utils.Extensions.displayToast
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    //binding
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    //others
    //to control the bottom nav
    private lateinit var navController: NavController

    @Inject
    lateinit var connectivityChecker: ConnectivityChecker
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //navigation and menu setup
        navController = findNavController(R.id.nav_host)
        //connecting our navBtm and navHost
        binding.btmNavigation.setupWithNavController(navController)

        //controlling the btm nav
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.splashFragment) {
                binding.btmNavigation.visibility = View.GONE
            }
        }

        //controlling the navigation by different internet status
        connectivityChecker.observe(this@MainActivity) { isConnected ->
            //by being connected
            if (isConnected) {

                navController.addOnDestinationChangedListener { _, destination, _ ->
                    if (destination.id == R.id.splashFragment) {
                        binding.btmNavigation.visibility = View.GONE
                    } else {
                        binding.btmNavigation.visibility = View.VISIBLE
                    }
                }
            } else {
                //lost internet
                navController.navigate(R.id.splashFragment)
                this.displayToast(getString(R.string.no_connection))
                binding.btmNavigation.visibility = View.GONE
            }
        }
    }

    override fun onNavigateUp(): Boolean {
        return navController.navigateUp() || super.onNavigateUp()
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}