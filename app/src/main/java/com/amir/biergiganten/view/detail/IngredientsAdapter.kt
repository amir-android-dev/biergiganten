package com.amir.biergiganten.view.detail

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.amir.biergiganten.data.model.ResponseBeer.ResponseBeerItem.Ingredients.Malt
import com.amir.biergiganten.databinding.ItemReceipeBinding
import javax.inject.Inject

class IngredientsAdapter @Inject constructor() :
    ListAdapter<Malt, IngredientsAdapter.IngredientsViewHolder>(object :
        DiffUtil.ItemCallback<Malt>() {
        override fun areItemsTheSame(
            oldItem: Malt,
            newItem: Malt
        ): Boolean = oldItem == newItem

        override fun areContentsTheSame(
            oldItem: Malt,
            newItem: Malt
        ): Boolean = oldItem == newItem

    }) {

    private lateinit var binding: ItemReceipeBinding
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): IngredientsViewHolder {
        binding = ItemReceipeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return IngredientsViewHolder()
    }

    override fun onBindViewHolder(holder: IngredientsViewHolder, position: Int) {
        holder.bind(currentList[position])
    }


    inner class IngredientsViewHolder : ViewHolder(binding.root) {
        fun bind(item: Malt) {
            binding.name.append(item.name)
        }
    }


}