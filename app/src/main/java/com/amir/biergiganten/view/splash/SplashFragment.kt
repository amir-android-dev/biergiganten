package com.amir.biergiganten.view.splash

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.amir.biergiganten.R
import com.amir.biergiganten.databinding.FragmentSplashBinding
import com.amir.biergiganten.utils.ConnectivityChecker
import com.amir.biergiganten.utils.Extensions.displaySnackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SplashFragment : Fragment() {
    //binding
    private var _binding: FragmentSplashBinding? = null
    private val binding get() = _binding!!

    //internet
    @Inject
    lateinit var internet: ConnectivityChecker

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSplashBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        internet.observe(viewLifecycleOwner) {
            if (it) {
                lifecycleScope.launch {
                    delay(2000)
                    findNavController().navigate(R.id.homeFragment)
                }
            } else {
                requireView().displaySnackbar(getString(R.string.no_connection))
            }
        }

    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}