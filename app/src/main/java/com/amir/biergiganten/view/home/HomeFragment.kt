package com.amir.biergiganten.view.home

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import com.amir.biergiganten.databinding.FragmentHomeBinding
import com.amir.biergiganten.viewmodel.home.HomeViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {
    //binding
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    //others
    private val viewModel: HomeViewModel by viewModels()

    @Inject
    lateinit var adapter: BeerAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHomeBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //rv setup
        binding.rvBeers.adapter = adapter
        binding.rvBeers.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        //calling the api & collecting data
        lifecycleScope.launch {
            viewModel.beerList.collect {
                adapter.submitData(it)
            }
        }

        //loading
        lifecycleScope.launch {
            adapter.loadStateFlow.collect{
                val state= it.refresh
                binding.prLoading.isVisible = state is LoadState.Loading
            }
        }
        adapter.setOnItemClickListener {
            val action =
                HomeFragmentDirections.actionToDetailFragment(it.id)
            findNavController().navigate(action)
        }
    }


    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}