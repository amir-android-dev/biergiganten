package com.amir.biergiganten.view.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.amir.biergiganten.data.model.ResponseBeer.ResponseBeerItem
import com.amir.biergiganten.databinding.ItemBeerBinding
import javax.inject.Inject

class SearchAdaptor @Inject constructor() :
    ListAdapter<ResponseBeerItem, SearchAdaptor.SearchViewHolder>(object :
        DiffUtil.ItemCallback<ResponseBeerItem>() {
        override fun areItemsTheSame(oldItem: ResponseBeerItem, newItem: ResponseBeerItem) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: ResponseBeerItem, newItem: ResponseBeerItem) =
            oldItem == newItem
    }) {

    private lateinit var binding: ItemBeerBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        binding = ItemBeerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SearchViewHolder()
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    inner class SearchViewHolder : ViewHolder(binding.root) {
        fun bind(beer: ResponseBeerItem) {
            binding.ivBeer.load(beer.imageUrl) {
                crossfade(true)
                //  crossfade(200)
            }

            binding.tvName.append(beer.name)
            binding.tvAttenuationLevel.append("${beer.attenuationLevel}%")
            binding.tvFirstBrewed.append(beer.firstBrewed.toString())

            binding.root.setOnClickListener {
                onItemClickListener?.let { it(beer) }
            }
        }
    }

    private var onItemClickListener: ((ResponseBeerItem) -> Unit)? = null
    fun setOnItemClickListener(listener: (ResponseBeerItem) -> Unit) {
        onItemClickListener = listener
    }

}