package com.amir.biergiganten.view.search

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.amir.biergiganten.databinding.FragmentSearchBinding
import com.amir.biergiganten.utils.Extensions.displaySnackbar
import com.amir.biergiganten.utils.Extensions.setupRv
import com.amir.biergiganten.utils.NetworkRequest
import com.amir.biergiganten.viewmodel.search.SearchViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class SearchFragment : Fragment() {
    //binding
    private var _binding: FragmentSearchBinding? = null
    private val binding get() = _binding!!
    //others
    @Inject
    lateinit var adaptor: SearchAdaptor
    private val viewModel: SearchViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        _binding = FragmentSearchBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.etSearch.doAfterTextChanged {
            if (it.toString().isEmpty()) {
                binding.ivEmpty.visibility = View.VISIBLE
                binding.rvSearch.visibility = View.GONE
            } else {
                binding.ivEmpty.visibility = View.GONE
                binding.rvSearch.visibility = View.VISIBLE

                //due to documentation ->if you need to add spaces just add an underscore (_).
                val nameWithUnderscores = it.toString().replace(" ", "_")
                viewModel.responseBeersBySearch(nameWithUnderscores)
                viewModel.responseBeersBySearch.observe(viewLifecycleOwner) { response ->

                    when (response) {
                        is NetworkRequest.Error -> requireView().displaySnackbar(response.msg.toString())
                        is NetworkRequest.Loading -> binding.prLoading.visibility = View.VISIBLE
                        is NetworkRequest.Success -> {
                            binding.prLoading.visibility = View.GONE
                            binding.rvSearch.setupRv(
                                adaptor,
                                LinearLayoutManager(
                                    requireContext(),
                                    LinearLayoutManager.VERTICAL,
                                    false
                                )
                            )
                            if(response.data?.isEmpty() == true){
                                binding.ivEmpty.visibility = View.VISIBLE
                                binding.rvSearch.visibility = View.GONE
                            }else{

                                adaptor.submitList(response.data)
                                binding.ivEmpty.visibility = View.GONE
                                binding.rvSearch.visibility = View.VISIBLE
                            }

                        }
                    }
                    adaptor.setOnItemClickListener {
                        val action = SearchFragmentDirections.actionToDetailFragment(it.id)
                        findNavController().navigate(action)
                    }
                }
            }
        }
    }
    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}