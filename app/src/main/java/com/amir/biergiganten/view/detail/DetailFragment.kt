package com.amir.biergiganten.view.detail

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.amir.biergiganten.data.model.ResponseBeer.ResponseBeerItem
import com.amir.biergiganten.databinding.FragmentDetailBinding
import com.amir.biergiganten.utils.Extensions.displaySnackbar
import com.amir.biergiganten.utils.NetworkRequest
import com.amir.biergiganten.viewmodel.detail.DetailViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class DetailFragment : Fragment() {
    //binding
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!

    //others
    private val args: DetailFragmentArgs by navArgs()
    private val viewModel: DetailViewModel by viewModels()

    @Inject
    lateinit var adapter: IngredientsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.fetchDetailResponse(args.id)
        viewModel.responseDetail.observe(viewLifecycleOwner) {
            when (it) {
                is NetworkRequest.Error -> requireView().displaySnackbar(it.msg.toString())
                is NetworkRequest.Loading -> binding.prLoading.visibility = View.VISIBLE
                //it returns a list with just one item
                is NetworkRequest.Success -> {
                    binding.prLoading.visibility = View.GONE
                    it.data?.first()?.let { beer -> setupView(beer) }
                }
            }
        }

        binding.ivBack.setOnClickListener {
            findNavController().popBackStack()
        }
    }

    private fun setupView(item: ResponseBeerItem) {
        binding.apply {
            ivPosterSmall.load(item.imageUrl)
            tvBeerName.append(item.name)
            tvAttenuationLevel.append("${item.attenuationLevel}%")
            tvDate.append(item.firstBrewed)
            tvBeerDescription.append(item.description)
            tvContributedBy.append(item.contributedBy)
            //chaining the food tips list
            val foodPairingText = item.foodPairing?.joinToString("\n")
            tvFoods.append(foodPairingText)
            tvTip.append(item.brewersTips)
            //setup adapter of ingredients
            rvIngredients.adapter = adapter
            rvIngredients.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
            adapter.submitList(item.ingredients?.malt)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}