package com.amir.biergiganten.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import coil.load
import com.amir.biergiganten.data.model.ResponseBeer.ResponseBeerItem
import com.amir.biergiganten.databinding.ItemBeerBinding
import com.amir.biergiganten.view.home.BeerAdapter.BeerViewHolder
import javax.inject.Inject

class BeerAdapter @Inject constructor() :
    PagingDataAdapter<ResponseBeerItem, BeerViewHolder>(diffCallback = differCallback) {

    private lateinit var binding: ItemBeerBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        //binding
        binding = ItemBeerBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return BeerViewHolder()
    }

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, position) }
        holder.setIsRecyclable(false)
    }


    inner class BeerViewHolder : ViewHolder(binding.root) {

        fun bind(beer: ResponseBeerItem, index: Int) {
            binding.ivBeer.load(beer.imageUrl) {
                crossfade(true)
                crossfade(200)
            }
            binding.tvName.append(beer.name)
            binding.tvAttenuationLevel.append("${beer.attenuationLevel}%")
            binding.tvFirstBrewed.append(beer.firstBrewed.toString())

            binding.root.setOnClickListener {
                onItemClickListener?.let { it(beer) }
            }
        }
    }

    private var onItemClickListener: ((ResponseBeerItem) -> Unit)? = null
    fun setOnItemClickListener(listener: (ResponseBeerItem) -> Unit) {
        onItemClickListener = listener
    }

    //creating the differCallBack
    companion object {
        val differCallback = object : DiffUtil.ItemCallback<ResponseBeerItem>() {
            override fun areItemsTheSame(
                oldItem: ResponseBeerItem,
                newItem: ResponseBeerItem
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ResponseBeerItem,
                newItem: ResponseBeerItem
            ): Boolean {
                return oldItem == newItem
            }

        }
    }


}