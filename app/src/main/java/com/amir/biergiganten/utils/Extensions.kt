package com.amir.biergiganten.utils

import android.content.Context
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import com.google.android.material.snackbar.Snackbar

object Extensions {

    fun View.displaySnackbar(msg: String) {
        Snackbar.make(this, msg, Snackbar.LENGTH_LONG).show()
    }

    fun Context.displayToast(msg: String){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show()
    }

    fun RecyclerView.setupRv(adapter: Adapter<*>, layoutManager: LinearLayoutManager){
        this.adapter = adapter
        this.layoutManager = layoutManager
    }
}