package com.amir.biergiganten.utils


//sealed class because every time I need just one of its states. It can be loading, error or success, all in once is impossible
sealed class NetworkRequest<T>(val data: T? = null, val msg: String? = null) {
    class Loading<T> : NetworkRequest<T>()

    class Success<T>(data: T) : NetworkRequest<T>(data = data)
    class Error<T>(data: T? = null, msg: String) : NetworkRequest<T>(data = data, msg = msg)

}
