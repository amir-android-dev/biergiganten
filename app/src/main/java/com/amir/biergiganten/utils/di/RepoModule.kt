package com.amir.biergiganten.utils.di

import com.amir.biergiganten.data.service.ApiService
import com.amir.biergiganten.viewmodel.detail.DetailRepo
import com.amir.biergiganten.viewmodel.detail.DetailRepoImpl
import com.amir.biergiganten.viewmodel.home.HomeRepo
import com.amir.biergiganten.viewmodel.home.HomeRepoImpl
import com.amir.biergiganten.viewmodel.search.SearchRepo
import com.amir.biergiganten.viewmodel.search.SearchRepoImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepoModule {

    @Provides
    @Singleton
    fun provideHomeRepo(apiService: ApiService): HomeRepo = HomeRepoImpl(apiService)

    @Provides
    @Singleton
    fun provideDetailRepo(apiService: ApiService): DetailRepo = DetailRepoImpl(apiService)

    @Provides
    @Singleton
    fun provideSearchRepo(apiService: ApiService): SearchRepo = SearchRepoImpl(apiService)
}