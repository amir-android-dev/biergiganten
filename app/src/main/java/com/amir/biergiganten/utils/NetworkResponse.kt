package com.amir.biergiganten.utils

import retrofit2.Response

class NetworkResponse<T>(private val response: Response<T>) {

    fun generalNetworkResponse(): NetworkRequest<T> {
        return when {
            response.message().contains("timeout") -> NetworkRequest.Error(msg = response.message())
            response.code() == 401 -> NetworkRequest.Error(msg = "You are not authorized")
            response.code() == 402 -> NetworkRequest.Error(msg = "Your free access is finished for today")
            response.code() == 422 -> NetworkRequest.Error(msg = "API KEY not found")
            response.code() == 400 -> NetworkRequest.Error(msg = "Ups, a parameter is passed without a value")
            response.code() in 300..399 -> NetworkRequest.Error(msg = response.message())
            response.code() in 500..599 -> NetworkRequest.Error(msg = "try later")
            response.isSuccessful -> NetworkRequest.Success(response.body()!!)
            else -> NetworkRequest.Error(msg = response.message())
        }
    }
}
/*
Informational responses (100 – 199)
Successful responses (200 – 299)
Redirection messages (300 – 399)
Client error responses (400 – 499)
Server error responses (500 – 599)
 */