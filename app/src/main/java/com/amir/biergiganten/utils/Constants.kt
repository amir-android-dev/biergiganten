package com.amir.biergiganten.utils

object Constants {

    const val BASE_URL = "https://api.punkapi.com/v2/"
    const val NETWORK_TIMEOUT = 60L
}