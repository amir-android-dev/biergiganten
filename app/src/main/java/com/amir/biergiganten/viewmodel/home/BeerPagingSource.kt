package com.amir.biergiganten.viewmodel.home

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.amir.biergiganten.data.model.ResponseBeer.ResponseBeerItem
import javax.inject.Inject

class BeerPagingSource @Inject constructor(private val repo: HomeRepo) :
    PagingSource<Int, ResponseBeerItem>() {
    override fun getRefreshKey(state: PagingState<Int, ResponseBeerItem>): Int? = null


    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ResponseBeerItem> {
        return try {
            val currentPage = params.key ?: 1
            val response = repo.responseBeers(currentPage)
            val beers = response.body() ?: emptyList()
            val responseBeers = mutableListOf<ResponseBeerItem>()
            responseBeers.addAll(beers)

            if (response.isSuccessful) {
                LoadResult.Page(
                    data = responseBeers,
                    prevKey = if (currentPage == 0) null else currentPage.minus(1),
                    nextKey = if (response.body()!!.isNotEmpty()) currentPage.plus(1) else null
                )
            } else {
                LoadResult.Error(Throwable(response.message()))
            }
        } catch (e: Exception) {
            LoadResult.Error(Throwable(e.message))
        }
    }
}