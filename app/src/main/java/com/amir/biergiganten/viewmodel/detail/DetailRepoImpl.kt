package com.amir.biergiganten.viewmodel.detail

import com.amir.biergiganten.data.service.ApiService
import javax.inject.Inject

class DetailRepoImpl @Inject constructor(private val api: ApiService) : DetailRepo {
    override suspend fun responseBeerDetail(id: Int) = api.responseBeerDetail(id)
}