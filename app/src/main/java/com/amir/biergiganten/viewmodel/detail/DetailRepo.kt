package com.amir.biergiganten.viewmodel.detail

import com.amir.biergiganten.data.model.ResponseBeer
import retrofit2.Response


interface DetailRepo {

    suspend fun responseBeerDetail(id: Int): Response<ResponseBeer>
}