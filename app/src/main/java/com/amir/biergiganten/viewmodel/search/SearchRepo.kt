package com.amir.biergiganten.viewmodel.search

import com.amir.biergiganten.data.model.ResponseBeer
import retrofit2.Response


interface SearchRepo {
    suspend fun responseSearchByName(name: String): Response<ResponseBeer>
}