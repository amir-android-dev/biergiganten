package com.amir.biergiganten.viewmodel.home

import com.amir.biergiganten.data.model.ResponseBeer
import retrofit2.Response


interface HomeRepo {
   suspend fun responseBeers(page: Int): Response<ResponseBeer>

}