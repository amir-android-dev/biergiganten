package com.amir.biergiganten.viewmodel.home


import com.amir.biergiganten.data.model.ResponseBeer
import com.amir.biergiganten.data.service.ApiService
import retrofit2.Response
import javax.inject.Inject

class HomeRepoImpl @Inject constructor(private val api: ApiService) : HomeRepo {
    override suspend fun responseBeers(page: Int): Response<ResponseBeer> = api.responseBeers(page)

}