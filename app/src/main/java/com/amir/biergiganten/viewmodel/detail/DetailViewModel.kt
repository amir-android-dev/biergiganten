package com.amir.biergiganten.viewmodel.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.amir.biergiganten.data.model.ResponseBeer
import com.amir.biergiganten.utils.NetworkRequest
import com.amir.biergiganten.utils.NetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val repo: DetailRepo) : ViewModel() {

    private var _responseDetail = MutableLiveData<NetworkRequest<ResponseBeer>>()
    val responseDetail get()  : LiveData<NetworkRequest<ResponseBeer>> = _responseDetail

    fun fetchDetailResponse(id:Int) = viewModelScope.launch {
        _responseDetail.value = NetworkRequest.Loading()
        val response = repo.responseBeerDetail(id)
        _responseDetail.value = NetworkResponse(response).generalNetworkResponse()
    }

}