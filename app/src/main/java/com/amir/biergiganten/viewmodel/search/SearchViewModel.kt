package com.amir.biergiganten.viewmodel.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.amir.biergiganten.data.model.ResponseBeer
import com.amir.biergiganten.utils.NetworkRequest
import com.amir.biergiganten.utils.NetworkResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SearchViewModel @Inject constructor(private val repo: SearchRepo) : ViewModel() {

    private var _responseBeersBySearch = MutableLiveData<NetworkRequest<ResponseBeer>>()
    val responseBeersBySearch get()  : LiveData<NetworkRequest<ResponseBeer>> = _responseBeersBySearch


    fun responseBeersBySearch(name: String) = viewModelScope.launch {
        _responseBeersBySearch.value = NetworkRequest.Loading()
        val response = repo.responseSearchByName(name)
        _responseBeersBySearch.value = NetworkResponse(response).generalNetworkResponse()
    }
}