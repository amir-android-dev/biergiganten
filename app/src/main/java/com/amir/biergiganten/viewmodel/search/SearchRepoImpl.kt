package com.amir.biergiganten.viewmodel.search

import com.amir.biergiganten.data.service.ApiService
import javax.inject.Inject

class SearchRepoImpl @Inject constructor(private val api: ApiService) : SearchRepo {

    override suspend fun responseSearchByName(name: String) = api.responseSearchByName(name)
}